# Changelog

### 2.0.15 (2024-10-16)

-   Added methods for migrating to email login and logging in with email.

### 2.0.14 (2024-09-13)

-   Changed test and staging url's for telenor

### 2.0.13 (2024-05-31)

-   Set width of payment portal to 350px

### 2.0.12 (2024-05-17)

-   Added amount and pointsAmount arguments to createOrder method

### 2.0.11 (2024-05-17)

-   Fixed response type for validatePhoneNumber method

### 2.0.10 (2024-05-03)

-   Added x-smartrefill-application header to all requests

### 2.0.9 (2024-04-25)

-   Added amountId as argument to validatePhoneNumber method

### 2.0.8 (2024-04-24)

-   Added methods to get and delete anonymous subscriptions

### 2.0.7 (2024-04-17)

-   Fixed return types that were wrong on a lot of methods

### 2.0.6 (2024-04-15)

-   Fixed getFailureMessage method by letting SDK user send in location as argument

### 2.0.5 (2024-04-12)

-   Fixed argument type for updating customer

### 2.0.4 (2024-04-12)

-   Fixed type on applicationVersionCode

### 2.0.3 (2024-04-12)

-   Fixed broken artifact

### 2.0.2 (2024-04-12)

-   Fixed broken types for all services and methods (BROKEN ARTIFACT)

### 2.0.1 (2024-04-11)

-   Excluded tests in build and replaced command in prebuild script

### 2.0.0 (2024-04-11)

-   Refactored everything to modularized structure. (EMPTY ARTIFACT, DO NOT DOWNLOAD)

### 1.0.26 (2024-03-13)

-   Updated method for validating number to new endpoint

### 1.0.25 (2024-03-12)

-   Fixed company parameter for CMS request and added some properties to Tags type

### 1.0.24 (2024-03-11)

-   Fixed company parameter for PMS request

### 1.0.23 (2024-03-11)

-   Made sessionToken to required property in Customer type

### 1.0.22 (2024-02-15)

-   Return proper response from subscription methods so that the SDK user can catch the errors from backend

### 1.0.21 (2024-02-13)

-   Fixed iframe height not being set correctly

### 1.0.20 (2024-01-29)

-   Added updateSubscription and deleteSubscription methods

### 1.0.19 (2024-01-16)

-   Added getSubscriptions method

### 1.0.18 (2024-01-16)

-   Added getOrderHistory method

### 1.0.17 (2024-01-15)

-   Added currentPassword to customer type

### 1.0.16 (2024-01-11)

-   Removed double slash from statusservice endpoint

### 1.0.15 (2024-01-10)

-   Fixed argument types for methods getPhone and deletePhone

### 1.0.14 (2024-01-03)

-   Fixed argument types for methods addPhone and updatePhone

### 1.0.13 (2023-12-27)

-   Changed username and phoneNumber property to optional in CustomerMutable type

### 1.0.12 (2023-12-22)

-   Fixed endpoint pathname in getPhone method

### 1.0.11 (2023-12-20)

-   Made phones array property required in Phone type

### 1.0.10 (2023-12-18)

-   Changed id from string to number on Phone- and Customer types

### 1.0.9 (2023-12-08)

-   Added timeAmountTextKey and extra text key to Tags type

### 1.0.8 (2023-12-08)

-   Created method for using voucher code

### 1.0.7 (2023-12-07)

-   Added sessionToken as optional argument to SDK config

### 1.0.6 (2023-12-04)

-   Added resetPassword method to AuthenticationService type

### 1.0.5 (2023-11-29)

-   Fixed request body in register function

### 1.0.4 (2023-11-29)

-   Added property extra1TextKey in Tags type

### 1.0.3 (2023-10-17)

-   Fixed correct customerID in Refill, CMS and PMS apis

### 1.0.2 (2023-10-16)

-   Fixed sdk-version header for api calls

### 1.0.1 (2023-10-12)

-   Removed gitlab_project_id that was no longer used

### 1.0.0 (2023-10-12)

-   Added npm publish command and publish-registry to gitlab

### 0.0.14 (2023-10-11)

-   Fixed typo in api-key header

### 0.0.13 (2023-10-09)

-   Removed resolving of payment portal inside the order methods. It should now be called by the SDK user manually.
-   Removed things related to reward top up since it is removed from Telenor and no longer used for any operator.

### 0.0.12 (2023-10-04)

-   Updated reame file
-   Update changelog file

### 0.0.11 (2023-10-04)

-   Changed the network request handling of promise, to that it resolves or reject correctly not just returning response and rejecting on failed request.

### 0.0.10 (2023-09-29)

![Static Badge](https://img.shields.io/badge/Breaking-Changes-orange.svg)

-   Network service is completely overhauled.
-   NetworkService is not extended by all services instead of implemented in constructure
-   Extending NetworkService exposes request method to all services
-   The request method allows for use of the Fetch API via cross-fetch.
-   Structure is still flat but has changes so that src -> resources contains the network service.
-   Resource directory contain service directory.
-   Service devided into content, products, refill and simregistration.
-   Refill contains all services dealing with refill calls.
-   This is to prepare the SDK for a more modular structure.

### 0.1.1 (2022-09-15)

-   All Services added in src folder with a flat structure
-   All Services implements calls to NetworkService.
