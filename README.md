# Refill SDK

## Implementation

First of all you need to install `@smartrefillsdk/web` as a dependency then make sure to import the SDK for initialization.

```ts
import { SmartRefillWebSDK } from '@smartrefillsdk/web';
```

## Initialization

To initializ the SDK you need to give it some configurations, known as CustomerParamConfig, these needs to be included param, when you initiate a new SmartRefillWebSDK.

```ts
const config: CustomerParamConfig = {
    customerID: string,
    apiKey: string,
    applicationVersionCode: number,
    applicationVersionName: string,
    environment: 'test' | 'staging' | 'production',
    language: 'sv' | 'en' | 'no' | 'fi' | 'lt' | 'ar',
    countryCode: 'SE' | 'NO' | 'FI' | 'LT',
};

export const client = new SmartRefillWebSDK(config);
```

## Usage

All parts of the platform can now be accessed via `client`, from which you can call its services and methods, see example below.

```ts
client.ProductModule.productService.getCatalogs();
```

This method will fetch all the catalogs and return them as an array.

### Modules and Services

-   ContentModule
    -   contentService
    -   helpers
-   ProductModule
    -   productService
    -   helpers
-   RefillModule
    -   customerService
    -   orderService
    -   subscriptionService
    -   receiptService
    -   cardRegistrationService
    -   authenticationService
    -   agreementService
    -   phoneService
    -   helpers
-   SimRegistrationModule
    -   simregistrationService
    -   helpers

## Documentation

Project includes docs folder with full documentation

## Author

Irina Novoselska

## Contributors

Malin Hanak, Simon Dellros, Irina Novoselska

## License

RefillSDK is propriety and can not be used with out explicit permission from Smart Refill AB. Refer to LICENSE.md for more info.
